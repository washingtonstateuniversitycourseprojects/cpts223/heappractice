#include "Heap.h"

#include <iostream>

#include <ctime>

#include <cstdlib>

int main()
{
    Heap<int> testHeap;

    std::srand(time(0));

    std::cout << "Inserting 1000 items into heap...\n" << std::endl;

    for (int i = 0; i < 1000; i++)
    {
        testHeap.push(rand() % 1000 + 1);
    }

    std::cout << "Insertion complete!\n" << std::endl;

    int j, k;

    do
    {
        do
        {
        
            std::cout << "What would you like to do?\n1. Print elements in heap\n2. Find kth item\n3. Exit\n" << std::endl;

            std::cin >> j;

        } while ((j < 1) || (j > 3));

        if (j == 1)
        {
            testHeap.print_pretty();

            std::cout << "\n" << std::endl;
        }
        else if (j == 2)
        {
            do
            {
                std::cout << "Enter a value for k: ";

                std::cin >> k;

                if ((k < 0) || (k > 999))
                {
                    std::cout << "That is not a valid value for k.\n" << std::endl;
                }

            } while ((k < 0) || (k > 999));

            testHeap.find_kth_item(k);
        }

    } while (j != 3);
}