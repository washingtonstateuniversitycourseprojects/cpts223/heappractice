prog: main.o
	g++ main.o -o runProg

main.o: main.cpp Heap.h
	g++ -c -g -Wall -std=c++11 main.cpp

clean: -rm *.o

run: @./runProgram